const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const messageSchema = new Schema({
    recipient: {
        type: String,
        required: true
    },
    message: {
        type: String,
        required: true
    },
    sender: {
        type: String,
        required: true
    },
    isSystem: {
        type: Boolean,
        required: true
    },
    datetime: {
        type: String,
        required: true
    }
});

const Message = mongoose.model('Message', messageSchema);

module.exports = Message;