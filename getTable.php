<?php

function getTable($conn)
{
    $sql = "SELECT students.id, 
               CONCAT(students.firstname, ' ', students.lastname) AS name,
               students.group_id,
               `groups`.name AS group_name, 
               students.gender_id,
               genders.name AS gender_name , 
               students.birthday,
               students.status
        FROM students 
        INNER JOIN `groups` ON students.group_id = `groups`.id 
        INNER JOIN genders ON students.gender_id = genders.id";

    return $conn->query($sql);
}

