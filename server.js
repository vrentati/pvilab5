const express = require("express");
const http = require("http");
const path = require("path");

const mongoose = require("mongoose");
const uri = "mongodb+srv://katerynavynokurovapz2022:wDFz6vWuZb5ZXuxY@cluster0.lck68ka.mongodb.net/?retryWrites=true&w=majority&appName=Cluster0";

mongoose
    .connect(uri)
    .then((res) => console.log("SUCCESS"))
    .catch((err) => console.log(err));

const conversationMessages = new Map();
const socketIo = require('socket.io');

const app = express();
const server = http.createServer(app);
const io = socketIo(server, {
    cors: {
        origin: "*", 
        methods: ["GET", "POST"] 
    }
});

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*'); 
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');
  next(); 
});

app.use(express.static(path.join(__dirname)));

const allUsers = [];
const activeUsers = [];

function saveMessage(conversationId, message) {
  if (!conversationMessages.has(conversationId)) {
    conversationMessages.set(conversationId, []);
  }
  conversationMessages.get(conversationId).push(message);
}

io.on("connection", function (socket) {
  socket.on("newuser", function (username) {
    socket.username = username;
    allUsers.push(username);
    activeUsers.push(username);
    socket.join("all"); 
    io.emit("refresh_users", allUsers,activeUsers);

    const msg = {
      recipient: "all",
      message: username + " " + "joined the conversation",
      sender: username,
      isSystem: true,
      datetime: new Date().toLocaleTimeString(),
    };
    const conversationId = "all";
    saveMessage(conversationId, msg);
    io.to(conversationId).emit("message", msg); 
  });

 

  socket.on("refresh_connections", function (usernames) {
    allUsers.forEach((activeUser) => {
      if (activeUser !== socket.username) {
        const roomName = [socket.username, activeUser].sort().join("_");
        socket.join(roomName);
      }
    });
  });

  socket.on("exituser", function (username) {
    activeUsers.splice(activeUsers.indexOf(username), 1);
    const msg = {
      recipient: "all",
      message: username + " " + "left the conversation",
      sender: username,
      isSystem: true,
      datetime: new Date().toLocaleTimeString(),
    };
    io.to("all").emit("message", msg);
    io.emit("refresh_users", allUsers,activeUsers);
  });


  socket.on("send_message", (data) => {
    const room =
      data.recipient === "all"
        ? "all"
        : [socket.username, data.recipient].sort().join("_");

    const message = {
      sender: data.sender,
      recipient: data.recipient,
      message: data.message,
      datetime: data.datetime,
      isSystem: data.isSystem,
      room: room,
    };

    io.to(room).emit("message", message);

    // Store message for the conversation
    const conversationId =
      data.recipient === "all"
        ? "all"
        : [socket.username, data.recipient].sort().join("_");

    saveMessage(conversationId, message);

    if (!allUsers.includes(data.recipient) && data.recipient !== "all") {
      const ofMsg = {
        sender: "System",
        username: socket.username,
        isSystem: true,
        recipient: socket.username,
        message: `User "${data.recipient}" is offline.`,
      };

      saveMessage(conversationId, ofMsg);
      io.to(room).emit("message", ofMsg); 
    }
  });

  socket.on("loadMessages", (recipient) => {

    const conversationId =
      recipient === "all"
        ? "all"
        : [socket.username, recipient].sort().join("_");

    if (conversationMessages.has(conversationId)) {
      const messages = conversationMessages.get(conversationId);

      messages.forEach((message) => {
        socket.emit("message", message);
      });
    }
  });
});


server.listen(3000, () => {
  console.log("Server is running on port 3000");
});
