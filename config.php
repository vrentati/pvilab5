<?php
include 'getData.php';
include 'getTable.php';

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "studentsweb";

$conn = new mysqli($servername, $username, $password, $dbname);

$genders = getGenders($conn);
$groups = getGroups($conn);

$result = getTable($conn);