$(document).ready(function () {
  const socket = io('http://localhost:3000');
  const app = $(".app");
  const messageContainer = app.find(".chat-messages");
  if (messageContainer.length)
    messageContainer.html("Please select the conversation to start chatting.");
  let selectedConversation = null;
  let uname;

  const messageInput = app.find("#msg");
  const sendButton = app.find(".btn");
  messageInput.addClass("hidden");
  sendButton.addClass("hidden");

  const joinPopup = app.find(".join-popup");
  joinPopup.addClass("active");

  app.find("#join-user").on("click", function (event) {
    event.preventDefault();
    const usernameInput = app.find("#username");
    const username = usernameInput.val().trim();

    if (username.length === 0) {
      return;
    }

    socket.emit("newuser", username);

    uname = username;
    const joinPopup = app.find(".join-popup");
    joinPopup.remove();
    app.find(".chat-screen").addClass("active");
  });

  app.find(".chat-screen #exit-chat").on("click", function () {
    socket.emit("exituser", uname);
    window.location.href = window.location.href;
  });

  app.find("#chat-form").on("submit", function (event) {
    event.preventDefault();
    const messageInput = app.find("#msg");
    const message = messageInput.val().trim();

    if (message.length === 0) {
      return;
    }

    if (message !== "") {
      if (selectedConversation) {
        socket.emit("send_message", {
          recipient: selectedConversation,
          message: message,
          sender: uname,
          datetime: new Date().toLocaleTimeString(),
        });
      }

      messageInput.val("");
    }
  });

  // Receive message
  socket.on("message", function (data) {
    const { sender, message, isSystem, recipient } = data;
    if (sender === uname && isSystem) {
      return;
    }
    if (
        sender === uname ||
        (selectedConversation === sender && data.recipient !== "all") ||
        (selectedConversation === "all" && data.recipient === "all")
    ) {
      renderMessage(sender === uname ? "my" : "other", {
        username: sender,
        text: message,
        datetime: data.datetime,
        isSystem: isSystem,
      });
    }

    if (
        !isSystem &&
        ((sender !== uname && selectedConversation !== recipient) ||
            (recipient === "all" && selectedConversation !== "all"))
    ) {
      showUnreadNotification(recipient === "all" ? "all" : sender);
    }
    if (
        selectedConversation === sender ||
        (selectedConversation === "all" && recipient === "all")
    ) {
      removeUnreadNotification(sender);
    }
  });

  socket.on("refresh_users", function (users, activeUsers) {
    addUsersToSidebar(users);
    socket.emit("refresh_connections", users);
    users.forEach((username) => {
      if (activeUsers.includes(username)) {
        const userElement = $("#user-" + username);
        if (userElement.length) {
          userElement.addClass("active-user");
        }
      } else {
        const userElement = $("#user-" + username);
        if (userElement.length) {
          userElement.removeClass("active-user");
        }
      }
    });

  });

  socket.on("userleft", function (username) {
    removeUserFromSidebar(username);
  });

  function addUsersToSidebar(users) {
    const userList = $("#users");
    if (userList.length) {
      userList.html("");
    }

    addSingleUser(userList, "all");
    users.forEach((username) => {
      if (username !== uname) {
        addSingleUser(userList, username);
      }
    });
  }

  function addSingleUser(userList, username) {
    const userItem = $("<div></div>").addClass("list-group-item list-group-item-action d-flex align-items-center").attr("id", "user-" + username);

    const avatarElement = $("<i></i>").addClass("bi bi-person-circle member-icon");
    userItem.append(avatarElement);

    const usernameElement = $("<span></span>").addClass("ms-2").text(username);
    userItem.append(usernameElement);

    userItem.on("click", function () {
      openIndividualChat(username);
      removeUnreadNotification(username);
      const allUserItems = $(".user-item");
      allUserItems.removeClass("selected");

      const userItem = $("#user-" + username);
      if (userItem.length) {
        userItem.addClass("selected");
      }
    });

    userList.append(userItem);
  }

  function openIndividualChat(username) {
    selectedConversation = username;

    const messageContainer = app.find(".chat-messages");
    if (messageContainer.length) messageContainer.html("");
    socket.emit("loadMessages", selectedConversation);

    //const chatForm = $("#chat-form");
    // chatForm.css("display", "block");
  }

  function renderMessage(type, message) {
    const messageContainer = app.find(".chat-messages");

    if (message.isSystem) {
      const el = $("<div></div>").addClass("update").text(message.text);
      messageContainer.append(el);
    } else if (type === "my" || type === "other") {
      const el = $("<div></div>").addClass("message");

      const now = new Date();
      const time = now.toLocaleTimeString([], {
        hour: "2-digit",
        minute: "2-digit",
      });

      el.html(`
                <div class="meta">${
          type === "my" ? "You" : message.username
      } <span>${time}</span></div>
                <p class="text">${message.text}</p>
            `);
      el.addClass(type === "my" ? "from-me" : "from-others");

      messageContainer.append(el);
    } else if (type === "left") {
      const el = $("<div></div>").addClass("update").text(message.text + " left the chat");
      messageContainer.append(el);
    }

    messageContainer.scrollTop(messageContainer.prop("scrollHeight"));
  }

  function showUnreadNotification(username) {
    const userItem = $("#user-" + username);
    if (userItem.length) {
      userItem.addClass("unread");
    }
  }
  function removeUnreadNotification(username) {
    const userItem = $("#user-" + username);
    if (userItem.length) {
      userItem.removeClass("unread");
    }
  }
});
