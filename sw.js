//importScripts('public/sw-cache.js');

// Install service worker
self.addEventListener('install', (event) => {
    event.waitUntil(
        caches.open('my-pwa-cache')
            .then((cache) => {
                return cache.addAll([
                    './',
                    '/public/index.html',
                    '/public/styles.css',
                    '/public/app.js'
                ]);
            })
    );
});

// Fetch resources with cache fallback
self.addEventListener('fetch', (event) => {
    event.respondWith(
        caches.match(event.request)
            .then((response) => {
                if (response) {
                    return response;
                }
                return fetch(event.request)
                    .then((response) => {
                        // Don't cache POST requests
                        if (event.request.method !== 'POST') {
                            return caches.open('my-pwa-cache')
                                .then((cache) => {
                                    cache.put(event.request, response.clone());
                                    return response;
                                });
                        } else {
                            return response;
                        }
                    });
            })
    );
});

// Update cache when new service worker is activated
self.addEventListener('activate', (event) => {
    event.waitUntil(
        caches.keys().then((cacheNames) => {
            return Promise.all(
                cacheNames.filter((cacheName) => {
                    return cacheName.startsWith('my-pwa-cache-') &&
                        cacheName !== 'my-pwa-cache-v1';
                }).map((cacheName) => {
                    return caches.delete(cacheName);
                })
            );
        })
    );
});
