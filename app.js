$(document).ready(function () {

    var allOptions = $('#group option').map(function () {
        return {value: $(this).val(), text: $(this).text()};
    }).get();
    var genderOptions = $('#gender option').map(function () {
        return {value: $(this).val(), text: $(this).text()};
    }).get();

    function addOrUpdateStudent(nextId) {
        var studentId = $("#studentId").val();
        var groupId = $("#group").val(); // Отримання числового ідентифікатора групи
        var group = allOptions.find(option => option.value === groupId);
        var groupName = group ? group.text : ""; // Отримання текстового значення групи

        var firstName = $("#firstName").val();
        var lastName = $("#lastName").val();
        var gender = $("#gender").val();
        var birthday = $("#birthday").val();
        var status = $("#status").is(":checked") ? "1" : "0";

        var newStudent = {
            id: studentId,
            group: groupId,
            firstName: firstName,
            lastName: lastName,
            gender: gender,
            birthday: birthday,
            status: status
        };

        var rowIndex = findRowIndexByStudentId(studentId);

        if (rowIndex !== -1) {
            // Якщо студент з таким ідентифікатором існує, замінюємо його дані
            var row = $("#studentTable tbody tr").eq(rowIndex);

            // Змініть значення в комірках на нові дані
            row.find('td:eq(1)').text(allOptions[newStudent.group].text);
            row.find('td:eq(2)').text(newStudent.firstName + ' ' + newStudent.lastName);
            row.find('td:eq(3)').text(genderOptions[newStudent.gender].text[0]);
            row.find('td:eq(4)').text(newStudent.birthday);
            row.find('td:eq(5)').find('.circle').removeClass('active').addClass(newStudent.status === "1" ? "active" : "");

            //row.data('student-id', newStudent.id);
            // updateTable();
        } else {
            // Якщо студент не існує, додаємо його до масиву
            newStudent.id = nextId;
            addToTable(newStudent);
        }

    }

    function findRowIndexByStudentId(studentId) {
        var rowIndex = -1; // Якщо рядок не знайдено, повернути -1

        var stId = parseInt(studentId);
        // Перебрати всі рядки таблиці
        $("#studentTable tbody tr").each(function (index) {
            // Отримати айді студента з поточного рядка
            var rowStudentId = parseInt($(this).data('student-id'));
            // Порівняти айді поточного студента з вказаним айді

            if (rowStudentId === stId) {
                // Якщо айді знайдено, зберегти індекс рядка та вийти з циклу
                rowIndex = index;
                return false; // Вихід з each()
            }
        });

        return rowIndex;
    }

    function sendDataToServer() {
        $.ajax({
            url: 'process.php', // URL адреса серверного скрипту
            type: 'POST', // Метод відправки даних
            dataType: 'json', // Тип отримуваної відповіді
            data: {
                confirmStudent: true,
                group: $("#group").val(),
                firstName: $("#firstName").val(),
                lastName: $("#lastName").val(),
                gender: $("#gender").val(),
                birthday: $("#birthday").val(),
                status: $("#status").is(":checked") ? 1 : 0,
                id: $("#studentId").val()
            },
            success: function (response) {
                $(".invalid-feedback").remove();
                $(".is-invalid").removeClass("is-invalid");
                if (response.success) {
                    $('#studentModal').modal('hide'); // Закриття модального вікна тільки при успішній відправці
                    addOrUpdateStudent(response.id);
                } else {
                    console.log('Error:', response.message);

                    // Відображення помилок біля полів вводу
                    for (var field in response.errors) {
                        $("#" + field).addClass("is-invalid").after('<div class="invalid-feedback">' + response.errors[field] + '</div>');
                    }
                }
            },
            error: function (xhr, status, error) {
                // Вивести повідомлення про помилку у випадку невдачі AJAX-запиту
                console.error('AJAX Error:', error);
            }
        })

    }

    // Функція для додавання нового студента до таблиці
    function addToTable(student) {
        var newRow = $("<tr>");

        // Додайте айді студента до рядка таблиці за допомогою методу data jQuery
        newRow.data('student-id', student.id);
        console.log(newRow.data('student-id'))
        console.log("nr: " + student.id);
        console.log(student)

        newRow.append('<td><input class="custom-checkbox2" type="checkbox"></td>');
        newRow.append('<td>' + allOptions[student.group].text + '</td>');
        newRow.append('<td>' + student.firstName + ' ' + student.lastName + '</td>');
        newRow.append('<td>' + genderOptions[student.gender].text[0] + '</td>');
        newRow.append('<td>' + student.birthday + '</td>');
        newRow.append('<td class="align-middle"><div class="circle ' + (student.status === "1" ? "active" : "") + '"></div></td>');
        newRow.append('<td><div class="d-flex justify-content-center align-items-center actionIcons">' +
            '<div class="d-flex justify-content-center align-items-center actionIcons">' +
            '<button class="icon-btn btn btn-outline-secondary btn-sm deleteStudentBtn" ' +
            'data-id="' + student.id + '"><i class="bi bi-x"></i></button>' +
            '<button class="icon-btn btn btn-outline-secondary btn-sm editStudentBtn"\n' +
            'data-id="' + student.id + '"><i class="bi bi-pencil"></i></button></div></div></td>');


        // Додати новий рядок до таблиці
        $("#studentTable tbody").append(newRow);
    }

    // Обробник події натискання кнопки збереження студента
    $(document).on('click', '#saveStudentBtn', function () {
        //addOrUpdateStudent();
        sendDataToServer();
        //$('#studentModal').modal('hide'); // Закриття модального вікна
    });

    $(document).on("click", ".editStudentBtn", function () {

        var newStudent = {
            id: "",
            group: "",
            firstName: "",
            lastName: "",
            gender: "",
            birthday: "",
            status: ""
        };
        var idS = $(this).data('id');
        let title = "Add student";

        if (idS) {
            var row = findRowIndexByStudentId(idS);
            if (row !== -1) {

                newStudent.id = $("#studentTable tbody tr").eq(row).data('student-id');

                // Знаходження тексту групи в об'єкті allOptions
                var groupText = $("#studentTable tbody tr").eq(row).find('td:eq(1)').text();

                // Пошук групи в об'єкті allOptions за текстом
                var group = allOptions.find(option => option.text === groupText);

                // Якщо група знайдена, отримати її ідентифікатор
                // Встановлення ідентифікатора групи для нового студента
                newStudent.group = group ? group.value : '';
                var text = $("#studentTable tbody tr").eq(row).find('td:eq(2)').text().split(" ");
                newStudent.firstName = text[0];
                newStudent.lastName = text[1];
                var genderText = $("#studentTable tbody tr").eq(row).find('td:eq(3)').text();
                var gender = genderOptions.find(option => option.text[0] === genderText[0]);
                newStudent.gender = gender ? gender.value : '';
                newStudent.birthday = $("#studentTable tbody tr").eq(row).find('td:eq(4)').text();
                var circleClass = $("#studentTable tbody tr").eq(row).find('td:eq(5)').attr('class');
                var hasActiveClassCorrect = circleClass.includes('active');
                newStudent.status = hasActiveClassCorrect ? "1" : "0";
                console.log(row)
                console.log(newStudent.status)

            }
            title = "Edit student";
        }
        console.log(newStudent);

        $("#studentId").val(newStudent.id);
        $("#group").val(newStudent.group);
        $("#firstName").val(newStudent.firstName);
        $("#lastName").val(newStudent.lastName);
        $("#gender").val(newStudent.gender);
        $("#birthday").val(newStudent.birthday);
        $("#status").prop("checked", newStudent.status === "1");

        $("#studentModalLabel").text(title);
        $('#studentModal').modal('show');

    });

    $(document).on('click', '.deleteStudentBtn', function () {
        var idS = $(this).data('id');
        console.log(idS);

        // Встановити айді студента в модальному вікні видалення
        $("#studentIdDel").val(idS);

        // Отримати дані рядка і встановити їх у повідомленні підтвердження видалення
        var studentName = $(this).closest("tr").find('td:eq(2)').text();
        $("#deleteUserMessage").text(`Are you sure you want to delete user ${studentName}?`);

        // Відкрити модальне вікно
        $("#deleteUserModal").modal('show');
    });


    // Обробник події натискання кнопки підтвердження видалення
    $(document).on('click', '#confirmDeleteBtn', function () {
        var studentId = $("#deleteUserModal").find("#studentIdDel").val(); // Отримати айді студента з модального вікна видалення
        console.log("Student ID:", studentId);

        $.ajax({
            url: 'process.php', // URL адреса серверного скрипту
            type: 'POST', // Метод відправки даних
            dataType: 'json', // Тип отримуваної відповіді
            data: {
                deleteStudent: true, // Позначка, що це запит на видалення студента
                studentId: studentId // ID студента, якого потрібно видалити
            },
            success: function(response) {
                // Обробка успішної відповіді від сервера
                $("#studentTable tbody tr").each(function () {
                    var id = parseInt(studentId);
                    console.log(studentId)
                    if ($(this).data('student-id') === id) {
                        console.log("RRRRRRRRRRRRRRRRRR")
                        $(this).remove();
                    }
                });
                console.log(response);
            },
            error: function(xhr, status, error) {
                // Обробка помилки при відправці запиту
                console.error('AJAX Error:', error);
            }
        });

        // Знайти рядок з айді студента та видалити його


        // Закрити модальне вікно видалення
        $("#deleteUserModal").modal('hide');
    });
});

